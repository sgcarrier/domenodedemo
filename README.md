# Welcome

This Dome Node Demo is used as an example project to show how to start using ROS. It acts as a simple dome that you can move around. You can monitor it with some useful ROS tools

---

# Getting started

First, you need to fix your dependencies:

## Depencencies
1. Ubuntu
2. Python2.7 (for ROS) and Python3.7 (for your code)
2. ROS melodic full (install info: https://wiki.ros.org/melodic/Installation/Ubuntu)
3. catkin (sudo apt-get install ros-melodic-catkin)
4. pip (sudo apt-get install python3-pip python-pip)
5. ```pip3 install roskpg catkin_pkg```
6. ```pip3 install empy```

## Clone this Repo

1. Click on the top right **Clone** button on this webpage
2. Copy the url that starts with "git clone ..."
3. Go in a terminal, cd to the location where you want to clone
4. Paste the url and click enter


## Create the catkin workspace

1. go into the cloned directory

``` cd domenodedemo ```

2. run catkin_make

``` catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_INCLUDE_DIR=/usr/include/python3.7m -DPYTHON_LIBRARY=/usr/lib/libpython3.7m.so ```
	
or simply
	
``` catkin_make```

The first one tells catkin you are coding in python3

---

# Running the node

## Start ROSCORE
Before starting any node, you need to start the roscore. This is the master node of the ros network.
In another terminal, run:

``` roscore```

## Start the node
While in your project you can start your node with the rosrun command. But first, you should source the setup.sh file in devel:

``` source ./devel/setup.sh ```

This makes it so your tools find the correct paths for your commands. Now you can start your node:

``` rosrun dome_node DomeNode.py ```

## Monitoring your node

This node use 2 thing to communicate with others: Services (http://wiki.ros.org/Services) and Topics (http://wiki.ros.org/Topics). To put it simply, services are a Request/Response type message: they are initiated by a request message, and must respond with an appropriate message. Topics are a bit simpler: they are simply variables that are broadcast to all that are interrested. A node "publishes" the value of a variable and other interrested nodes "subscribe" to this topics to receive the latest value.

For example, this node's service is to change the angle. Someone can request an angle change.
And, this node's topic is the state of the dome (current angle, if its rotating or not, etc).

You can write your own node to interact with this node, but first, it's easier to validate with tools that ROS provides.

rostopic allows you to listen in on a topic. In a new terminal:

``` rostopic echo /dome ```

rosservice allows you to interact with a service. In a new terminal:

```  rosservice call /angleChange 60 ```

**Note**: you might have to run the ``` source ./devel/setup.sh ``` command in these new terminals.

When you run this last command, you should see the dome moving on the terminal where you launched the rostopic echo


