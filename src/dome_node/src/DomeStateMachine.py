
from dome_node.srv import angleChange, angleChangeResponse
from dome_node.msg import DomeStatus
import rospy

class DomeStateMachine:
    """

    """

    """ Here I define the possible states the Dome can have.
        The systems starts with PARKED and CLOSED window"""
    windowStates = ['CLOSED', 'OPEN', 'CLOSING', 'OPENING', 'ERROR']
    motorStates = ["PARKED", "ROTATING", "TRACKING"]

    def __init__(self):
        """Initiatial config"""

        """Private variables"""
        self.__running = False
        rospy.init_node('DomeNode', anonymous=True)
        self._windowState = 'CLOSED'
        self._motorState = 'PARKED'
        self._currentAngle = 0
        self._aimedAngle = 0

        """State publisher"""

        self.pub = rospy.Publisher('dome',DomeStatus, queue_size=10)

    def run(self):
        self.__running = True
        rate = rospy.Rate(1)
        print("Started the dome")
        while self.__running and not rospy.is_shutdown():
            if self._motorState is 'PARKED':
                if self._currentAngle != self._aimedAngle:
                    self._motorState = 'ROTATING'
                    self.pub.publish(self._windowState, self._motorState, self._currentAngle, self._aimedAngle)
            elif self._motorState is 'ROTATING':
                if self._currentAngle != self._aimedAngle:
                    if self._currentAngle > self._aimedAngle:
                        self._currentAngle += max(-5, self._aimedAngle - self._currentAngle)
                    else:
                        self._currentAngle += min(5, self._aimedAngle - self._currentAngle)
                else:
                    self._motorState = 'PARKED'
                self.pub.publish(self._windowState, self._motorState, self._currentAngle, self._aimedAngle)
            elif self._motorState is 'TRACKING':
                self._motorState = 'PARKED'
            else:
                print('Error, putting the dome to parked')
                self._motorState = 'PARKED'

            rate.sleep()

    def setAngle(self, params):
        if (0 <= params.angle) and (params.angle <= 360):
            if self._aimedAngle != params.angle:
                self._aimedAngle = params.angle
                self.pub.publish(self._windowState, self._motorState, self._currentAngle, self._aimedAngle)

        return angleChangeResponse(0)


    def stop(self):
        self.__running = False

def sendMotorCommand(newAngle):
    pass
