#!/usr/bin/env python
import rospy


def DomeQuickMonitor():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('DomeQuickMonitor', anonymous=True)

    print("All available params: ")
    print(rospy.get_param_names())

    # spin() simply keeps python from exiting until this node is stopped
    rate = rospy.Rate(10)  # 10hz
    while not rospy.is_shutdown():
        status = "WindowState: {0}, MotorState: {1}, Angle : {2}, DestAngle: {3}".format(rospy.get_param('/dome/windowState'),
                                                                                         rospy.get_param('/dome/motorState'),
                                                                                         rospy.get_param('/dome/angle'),
                                                                                         rospy.get_param('/dome/destAngle'))
        print(status)
        rate.sleep()


if __name__ == '__main__':
    DomeQuickMonitor()