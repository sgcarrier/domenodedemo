import rospy
from dome_node.srv import angleChange, angleChangeRequest



rospy.wait_for_service('angleChange')
add_two_ints = rospy.ServiceProxy('angleChange', angleChange)
try:
  resp1 = angleChange(60)
except rospy.ServiceException as exc:
  print("Service did not process request: " + str(exc))