#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
from dome_node.srv import angleChange

import DomeStateMachine

def DomeNode():
    # windowState = rospy.set_param('windowState', 'CLOSED')
    # motorState = rospy.set_param('motorState', 'PARKED')
    # domeAngle = rospy.set_param('angle', 0)
    # DestAngle = rospy.set_param('destAngle', 0)

    DSM = DomeStateMachine.DomeStateMachine()
    s = rospy.Service('angleChange', angleChange, DSM.setAngle)

    rospy.init_node('DomeNode', anonymous=True)

    DSM.run()

    #rospy.spin()


if __name__ == '__main__':
    try:
        DomeNode()
    except rospy.ROSInterruptException:
        pass